<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:92:"D:\web\me_h_ul_admin\10-25\me_h_ul_admin\public/../application/admin\view\user\user-del.html";i:1540458181;s:81:"D:\web\me_h_ul_admin\10-25\me_h_ul_admin\application\admin\view\common\blank.html";i:1540519084;s:82:"D:\web\me_h_ul_admin\10-25\me_h_ul_admin\application\admin\view\common\footer.html";i:1540432916;}*/ ?>
﻿﻿<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="Bookmark" href="/favicon.ico" >
<link rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="/static/lib/html5shiv.js"></script>
<script type="text/javascript" src="/static/lib/respond.min.js"></script>

<!--1-图片上传的引入文件-->
<link href="/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/static/bootstrap/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
<script src="/static/bootstrap/js/jquery-2.0.3.min.js"></script>
<script src="/static/bootstrap/js/fileinput.js" type="text/javascript"></script>
<script src="/static/bootstrap/js/fileinput_locale_de.js" type="text/javascript"></script>
<script src="/static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>


<link href="/static/static/h-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
<link href="/static/static/h-ui.admin/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
<link href="/static/lib/Hui-iconfont/1.0.8/iconfont.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="/static/static/h-ui.admin/css/style.css" />
<!--[if IE 6]>

<!--引入echarts 数据图-->
 <script src="/static/lib/echarts/echarts.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="/static/lib/DD_belatedPNG_0.0.8a-min.js" ></script>-->
<!--<script>DD_belatedPNG.fix('*');</script>-->

<![endif]-->
<title>删除的用户</title>
</head>
<body>
<nav class="breadcrumb">
	<i class="Hui-iconfont">&#xe67f;</i>
	首页 <span class="c-gray en">&gt;</span>
	用户中心 <span class="c-gray en">&gt;</span>
	删除的用户<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" >
	<i class="Hui-iconfont">&#xe68f;</i>
	</a>
</nav>
<div class="page-container">
	<div class="text-c"> 日期范围：
		<input type="text" class="input-text" style="width:250px" placeholder="输入会员名称、电话、邮箱" id="" name="">
		<button type="submit" class="btn btn-success radius" id="" name=""><i class="Hui-iconfont">&#xe665;</i> 搜用户</button>
	</div>
	<div class="cl pd-5 bg-1 bk-gray mt-20">
		<span class="l">
			<a href="javascript:;" onclick="datadel()" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe6e2;</i>
			批量删除</a>
		</span>
		<span class="r">共有数据：<strong><?php echo $num; ?></strong> 条</span> </div>
	<div class="mt-20">
		<table class="table table-border table-bordered table-hover table-bg table-sort">
			<thead>
			<tr class="text-c">
				<th width="25"><input type="checkbox" name="" value=""></th>
				<th width="80">ID</th>
				<th width="90">头像</th>
				<th width="80">会员名</th>
				<th width="90">手机</th>
				<th width="100">邮箱</th>
				<th width="100">登录的ip</th>
				<th width="100">登录的时间</th>
				<th width="130">添加的时间</th>
				<th width="130">更新的时间</th>
				<th width="70">状态</th>
				<th width="100">操作</th>
			</tr>
			</thead>
			<tbody>
			<?php if(is_array($data) || $data instanceof \think\Collection || $data instanceof \think\Paginator): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$user): $mod = ($i % 2 );++$i;?>
			<tr class="text-c">
				<td><input type="checkbox" value="1" name=""></td>
				<td><?php echo $user['id']; ?></td>
				<td><img src="<?php echo $user['user_logo']; ?>" alt="" width="50" height="50"></td>
				<td><?php echo $user['user_name']; ?></td>
				<td><?php echo $user['phone']; ?></td>
				<td><?php echo $user['email']; ?></td>
				<td><?php echo $user['last_ip']; ?></td>
				<td><?php echo date('Y-m-d H:s:i',$user['last_time']); ?></td>
				<td><?php echo date('Y-m-d H:s:i',$user['add_time']); ?></td>
				<td><?php echo date('Y-m-d H:s:i',$user['update_time']); ?></td>
				<?php if($user['status'] == 1): ?>
				<td class="td-status"><span class="label label-success radius">已启用</span></td>
				<?php else: ?>
				<td class="td-status"><span class="label label-default radius">已禁用</span></td>
				<?php endif; ?>
				<td class="td-manage">
					<a title="还原" href="javascript:;" onclick="member_del(this,'<?php echo $user['id']; ?>')" class="ml-5" style="text-decoration:none">
						<i class="Hui-iconfont">&#xe66b;</i>
					</a>
				</td>
			</tr>
			<?php endforeach; endif; else: echo "" ;endif; ?>
			</tbody>
		</table>
	</div>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/static/lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="/static/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript" src="/static/lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="/static/lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript" src="/static/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/static/h-ui.admin/js/H-ui.admin.js"></script>
<!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/static/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/static/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/static/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
$(function(){
	$('.table-sort').dataTable({
		"aaSorting": [[ 1, "desc" ]],//默认第几个排序
		"bStateSave": true,//状态保存
		"aoColumnDefs": [
		  //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
		  {"orderable":false,"aTargets":[0,8,9]}// 制定列不参与排序
		]
	});
});

/*用户-还原*/
function member_del(obj,id){
    layer.confirm('确认要还原吗？',function(index){
        $.post(
            "<?php echo url('admin/user/reduction'); ?>",
            {id:id},
            function(data){
                var data = JSON.parse(data);
                if(data.status==1){
                    $(obj).parents("tr").remove();
                    layer.msg('已还原!',{icon:1,time:1000});
                }else{
                    var msg = data.msg;
                    layer.msg(msg,{icon:2,time:2000});
                }
            });
    });
}
</script> 
</body>
</html>