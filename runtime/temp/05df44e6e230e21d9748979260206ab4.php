<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:99:"D:\web\me_h_ul_admin\10-25\me_h_ul_admin\public/../application/admin\view\echarts\echarts-user.html";i:1540536742;s:81:"D:\web\me_h_ul_admin\10-25\me_h_ul_admin\application\admin\view\common\blank.html";i:1540519084;s:82:"D:\web\me_h_ul_admin\10-25\me_h_ul_admin\application\admin\view\common\footer.html";i:1540432916;}*/ ?>
﻿<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="Bookmark" href="/favicon.ico" >
<link rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="/static/lib/html5shiv.js"></script>
<script type="text/javascript" src="/static/lib/respond.min.js"></script>

<!--1-图片上传的引入文件-->
<link href="/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/static/bootstrap/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
<script src="/static/bootstrap/js/jquery-2.0.3.min.js"></script>
<script src="/static/bootstrap/js/fileinput.js" type="text/javascript"></script>
<script src="/static/bootstrap/js/fileinput_locale_de.js" type="text/javascript"></script>
<script src="/static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>


<link href="/static/static/h-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
<link href="/static/static/h-ui.admin/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
<link href="/static/lib/Hui-iconfont/1.0.8/iconfont.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="/static/static/h-ui.admin/css/style.css" />
<!--[if IE 6]>

<!--引入echarts 数据图-->
 <script src="/static/lib/echarts/echarts.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="/static/lib/DD_belatedPNG_0.0.8a-min.js" ></script>-->
<!--<script>DD_belatedPNG.fix('*');</script>-->

<![endif]-->
<title>用户管理</title>
</head>
<body>
<nav class="breadcrumb">
    <i class="Hui-iconfont">&#xe67f;</i>
    首页 <span class="c-gray en">&gt;</span>
    系统统计 <span class="c-gray en">&gt;</span>
    用户数据<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" >
    <i class="Hui-iconfont">&#xe68f;</i>
</a>
</nav>
<div class="page-container">
    <body>
    <!-- 为 ECharts 准备一个具备大小（宽高）的 DOM -->
    <div id="main" style="width: 600px;height:400px;"></div>
    <div id="system" style="width: 600px;height:400px;"></div>
    </body>
</div>
<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/static/lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="/static/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript" src="/static/lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="/static/lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript" src="/static/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/static/h-ui.admin/js/H-ui.admin.js"></script>
<!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/static/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="/static/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/static/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('main'));

    // 指定图表的配置项和数据
    var option = {
        title: {
            text: '年龄柱状图'
        },
        tooltip: {},
        legend: {
            data:['年龄']
        },
        xAxis: {
            name:"姓名",
            data: [<?php if(is_array($data) || $data instanceof \think\Collection || $data instanceof \think\Paginator): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$info): $mod = ($i % 2 );++$i;?>"<?php echo $info["user_name"]; ?>",<?php endforeach; endif; else: echo "" ;endif; ?>]
        },
        yAxis: {},
        series: [{
            name: '年龄',
            type: 'bar',
            data: [<?php if(is_array($data) || $data instanceof \think\Collection || $data instanceof \think\Paginator): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$info2): $mod = ($i % 2 );++$i;?><?php echo $info2["age"]; ?>,<?php endforeach; endif; else: echo "" ;endif; ?>]
        }]
    };
    // 使用刚指定的配置项和数据显示图表
    myChart.setOption(option);

    var myChart2 = echarts.init(document.getElementById('system'));
    var option2 = {
        title : {
            text: '该站点用户访问来源',
            subtext: '实时数据',
            x:'center'
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            left: 'left',
            data: [<?php if(is_array($sql) || $sql instanceof \think\Collection || $sql instanceof \think\Paginator): $i = 0; $__LIST__ = $sql;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sql2): $mod = ($i % 2 );++$i;?>"<?php echo $sql2['user_system']; ?>",<?php endforeach; endif; else: echo "" ;endif; ?>]
        },
        series : [
            {
                name: '访问来源',
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[<?php if(is_array($sql) || $sql instanceof \think\Collection || $sql instanceof \think\Paginator): $i = 0; $__LIST__ = $sql;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sql1): $mod = ($i % 2 );++$i;?>{value:<?php echo $sql1["num"]; ?>,name:"<?php echo $sql1['user_system']; ?>"},<?php endforeach; endif; else: echo "" ;endif; ?>]
            }
        ]
    };
    myChart2.setOption(option2);
</script>
</body>
</html>