<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:100:"D:\web\me_h_ul_admin\10-25\me_h_ul_admin\public/../application/admin\view\admin\admin-role-edit.html";i:1540432916;s:81:"D:\web\me_h_ul_admin\10-25\me_h_ul_admin\application\admin\view\common\blank.html";i:1540519084;s:82:"D:\web\me_h_ul_admin\10-25\me_h_ul_admin\application\admin\view\common\footer.html";i:1540432916;}*/ ?>
﻿﻿<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="Bookmark" href="/favicon.ico" >
<link rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="/static/lib/html5shiv.js"></script>
<script type="text/javascript" src="/static/lib/respond.min.js"></script>

<!--1-图片上传的引入文件-->
<link href="/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/static/bootstrap/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
<script src="/static/bootstrap/js/jquery-2.0.3.min.js"></script>
<script src="/static/bootstrap/js/fileinput.js" type="text/javascript"></script>
<script src="/static/bootstrap/js/fileinput_locale_de.js" type="text/javascript"></script>
<script src="/static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>


<link href="/static/static/h-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
<link href="/static/static/h-ui.admin/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
<link href="/static/lib/Hui-iconfont/1.0.8/iconfont.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="/static/static/h-ui.admin/css/style.css" />
<!--[if IE 6]>

<!--引入echarts 数据图-->
 <script src="/static/lib/echarts/echarts.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="/static/lib/DD_belatedPNG_0.0.8a-min.js" ></script>-->
<!--<script>DD_belatedPNG.fix('*');</script>-->

<![endif]-->
<!--/meta 作为公共模版分离出去-->

<title>编辑网站角色 - 管理员管理 - H-ui.admin v3.1</title>
<meta name="keywords" content="H-ui.admin v3.1,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
<meta name="description" content="H-ui.admin v3.1，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>
<body>
<article class="page-container">
	<form action="" method="post" class="form form-horizontal" id="form-admin-role-add">
		<div class="row cl">
			<!--隐藏域 id-->
			<input type="hidden" name="role_id" id="role_id" value="<?php echo $data['role_id']; ?>">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>角色名称：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $data['role_name']; ?>" placeholder="" id="roleName" name="roleName">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3">备注：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $data['desc']; ?>" placeholder="" id="desc" name="desc">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3">网站角色：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<?php if(is_array($admins) || $admins instanceof \think\Collection || $admins instanceof \think\Paginator): $i = 0; $__LIST__ = $admins;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$admin): $mod = ($i % 2 );++$i;?>
				<dl class="permission-list">
					<dt>
						<label>
							<input type="checkbox" value="<?php echo $admin['id']; ?>"  name="menuid" id="user-Character-0" <?php if($data and in_array($admin['id'],$data['menu'])): ?>checked<?php endif; ?>>
							<?php echo $admin['name']; ?></label>
					</dt>
					<dd>
						<?php if(!empty($admin['cmenu']) == true): if(is_array($admin['cmenu']) || $admin['cmenu'] instanceof \think\Collection || $admin['cmenu'] instanceof \think\Paginator): $i = 0; $__LIST__ = $admin['cmenu'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vv): $mod = ($i % 2 );++$i;?>
						<dl class="cl permission-list2">
							<dt>
								<label class="">
									<input type="checkbox" value="<?php echo $vv['id']; ?>" name="menuid" id="user-Character-0-0" <?php if($data and in_array($vv['id'],$data['menu'])): ?>checked<?php endif; ?>>
									<?php echo $vv['name']; ?></label>
							</dt>
							<?php if(!empty($vv['per']) == true): ?>
							<dd>
								<?php if(is_array($vv['per']) || $vv['per'] instanceof \think\Collection || $vv['per'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vv['per'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vvv): $mod = ($i % 2 );++$i;?>
								<label class="">
									<input type="checkbox" value="<?php echo $vvv['id']; ?>" name="menuid" id="user-Character-0-0-0" <?php if($data and in_array($vvv['id'],$data['menu'])): ?>checked<?php endif; ?>>
									<?php echo $vvv['name']; ?></label>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</dd>
							<?php endif; ?>
						</dl>
						<?php endforeach; endif; else: echo "" ;endif; endif; ?>
					</dd>
				</dl>
				<?php endforeach; endif; else: echo "" ;endif; ?>
			</div>
		</div>
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
				<button type="submit" class="btn btn-success radius" id="admin-role-save" name="admin-role-save">
					<i class="icon-ok"></i> 确定</button>
			</div>
		</div>
	</form>
</article>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="/static/lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="/static/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/static/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript" src="/static/lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="/static/lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript" src="/static/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/static/static/h-ui.admin/js/H-ui.admin.js"></script>
<!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="/static/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript" src="/static/lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="/static/lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript">
    $(function(){
        $(".permission-list dt input:checkbox").click(function(){
            $(this).closest("dl").find("dd input:checkbox").prop("checked",$(this).prop("checked"));
        });

        $("#form-admin-role-add").validate({
            rules:{
                roleName:{
                    required:true,
                },
            },
            onkeyup:false,
            focusCleanup:true,
            success:"valid",
            submitHandler:function(form){
                // 	$(form).ajaxSubmit();
                // 	var index = parent.layer.getFrameIndex(window.name);
                // 	parent.layer.close(index);
            }

        });

        $('#admin-role-save').click(function(){
            //js获取选中的checkbox的value值
            // obj = document.getElementsByName("menuid");
            //  check_val = [];
            //  for(k in obj){
            //      if(obj[k].checked)
            //          check_val.push(obj[k].value);
            //  }
            //  alert(check_val);
            var tmp = $("input[type='checkbox']").is(':checked');
            var name = $.trim($('#roleName').val());
            var desc = $.trim($('#desc').val());
            var reg = /^[\u4E00-\u9FA5]{1,5}$/;
            if (name=='') {
                layer.msg('角色名称不能为空',{icon:2,time:2000});
                // alert('角色名称不能为空');
                return false;
            }
            if(!reg.test(name)||!reg.test(desc)) {
                layer.msg('名称和描述只支持中文字符',{icon:2,time:2000});
                // alert("名称和描述只支持中文字符");
                return false;
            }
            if (tmp==false) {
                layer.msg('请选择网站角色',{icon:2,time:2000});
                // alert('请选择网站角色');
                return false;
            }

            //jq获取checkbox选中的value的值
            var menuid = "";
            $("input[name=menuid]").each(function(i){
                if(this.checked) {
                    if(0==i){
                        menuid = $(this).val();
                    }else{
                        menuid += (","+$(this).val());
                    }
                }
            });
            //获取隐藏域的id、
			var role_id=$("#role_id").val();
            $.post(
                "<?php echo url('admin/admin/editRole'); ?>",
                {menuid:menuid,desc:desc,name:name,role_id:role_id},
                function (dat) {
                    var data = JSON.parse(dat);
                    var msg = data.msg;
                    if(data.status == 1){
                        layer.msg(msg, {
                            icon: 1,
                            time: 2000 //2秒关闭（如果不配置，默认是3秒）
                        }, function(){
                            var index = parent.layer.getFrameIndex(window.name);
                            parent.$('.btn-refresh').click();
                            window.parent.location.reload();
                            parent.layer.close(index);
                        });
                    }else{
                        layer.msg(msg,{icon:2,time:2000});
                    }
                });

        });
    });
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>