/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : hul_admin

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-10-26 15:21:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `names` varchar(60) NOT NULL DEFAULT '' COMMENT '用户名',
  `email` varchar(200) DEFAULT '' COMMENT 'email',
  `password` varchar(200) DEFAULT '' COMMENT '密码',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `last_login` varchar(50) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `last_ip` varchar(50) NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `phone` varchar(14) DEFAULT NULL,
  `role_id` varchar(50) NOT NULL DEFAULT '0' COMMENT '角色id',
  `status` tinyint(10) NOT NULL DEFAULT '1' COMMENT '状态 1正常，2冻结',
  `num` int(11) DEFAULT NULL COMMENT '登录的次数',
  PRIMARY KEY (`id`),
  KEY `user_name` (`names`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', '2235@qq.com', 'e9q0ru/s1vmWU', '1537869280', '2018-10-26 13:29:28', '127.0.0.1', '1878276', '1', '1', '6');
INSERT INTO `admin` VALUES ('18', 'admina', 'admin@qq.com', '92KM/a7.wqDkc', '1533268697', '2018-09-13 16:25:40', '127.0.0.1', '18782737798', '2', '1', null);
INSERT INTO `admin` VALUES ('21', 'he5je', 'admin@qq.com', 'e9q0ru/s1vmWU', '1534994944', '0', '', '18782737798', '3', '1', null);
INSERT INTO `admin` VALUES ('22', 'adminww', 'admin@qq.com', 'dfh4Ak1RKdnyE', '1537421688', '0', '', '18782737798', '1', '1', null);

-- ----------------------------
-- Table structure for auth_menu
-- ----------------------------
DROP TABLE IF EXISTS `auth_menu`;
CREATE TABLE `auth_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '菜单名',
  `parent_id` tinyint(4) NOT NULL DEFAULT '0' COMMENT '父级ID, 0为顶级菜单',
  `status` tinyint(4) DEFAULT '1' COMMENT '1表示显示，2表示隐藏',
  `url` varchar(30) DEFAULT NULL,
  `sort` tinyint(4) DEFAULT '0' COMMENT '排序',
  `type` varchar(10) NOT NULL DEFAULT 'menu' COMMENT '类型 menu表示菜单栏控制，per表示节点控制',
  `ico` varchar(255) DEFAULT NULL COMMENT '图标',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_menu
-- ----------------------------
INSERT INTO `auth_menu` VALUES ('1', '管理员管理', '0', '1', null, '0', 'menu', '&#xe62d;');
INSERT INTO `auth_menu` VALUES ('2', '菜单栏管理', '0', '1', null, '0', 'menu', '&#xe72b;');
INSERT INTO `auth_menu` VALUES ('3', '角色管理', '1', '1', 'admin/admin/role', '0', 'menu', null);
INSERT INTO `auth_menu` VALUES ('4', '权限管理', '1', '1', 'admin/admin/permission', '0', 'menu', null);
INSERT INTO `auth_menu` VALUES ('5', '管理员列表', '1', '1', 'admin/admin/index', '0', 'menu', null);
INSERT INTO `auth_menu` VALUES ('6', '菜单栏', '2', '1', 'admin/menu/index', '0', 'menu', null);
INSERT INTO `auth_menu` VALUES ('7', '添加管理员', '5', '1', 'admin/admin/addAdmin', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('8', '删除管理员', '5', '1', 'admin/admin/delAdmin', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('9', '修改管理员状态', '5', '1', 'admin/admin/updateAdminStatus', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('10', '修改管理员', '5', '1', 'admin/admin/editAdmin', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('11', '添加角色', '3', '1', 'admin/admin/addRole', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('12', '编辑角色', '3', '1', 'admin/admin/editRole', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('13', '删除角色', '3', '1', 'admin/admin/delRole', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('14', '添加权限', '4', '1', 'admin/admin/addPermission', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('15', '编辑权限', '4', '1', 'admin/admin/editPermission', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('16', '删除权限', '4', '1', 'admin/admin/delPermission', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('17', '添加菜单栏', '6', '1', 'admin/menu/addMenu', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('18', '修改菜单栏', '6', '1', 'admin/menu/editMenu', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('20', '删除菜单栏', '6', '1', 'admin/menu/delMenu', '1', 'per', null);
INSERT INTO `auth_menu` VALUES ('26', '系统管理', '0', '1', null, '0', 'menu', '&#xe63c;');
INSERT INTO `auth_menu` VALUES ('32', '日志列表', '26', '1', 'admin/system/operation', null, 'menu', null);
INSERT INTO `auth_menu` VALUES ('34', '删除所有', '32', '1', 'admin/system/delOperation', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('35', '清空缓存', '32', '1', 'admin/base/clear', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('36', '会员管理', '0', '1', null, null, 'menu', '&#xe6cc;');
INSERT INTO `auth_menu` VALUES ('37', '会员列表', '36', '1', 'admin/user/index', null, 'menu', '');
INSERT INTO `auth_menu` VALUES ('38', '会员回收站', '36', '1', 'admin/user/recovery', null, 'menu', '');
INSERT INTO `auth_menu` VALUES ('39', '会员的添加', '37', '1', 'admin/user/addUser', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('40', '会员的编辑', '37', '1', 'admin/user/editUser', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('41', '会员的删除', '37', '1', 'admin/user/delUser', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('42', '会员的还原', '38', '1', 'admin/user/reduction', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('43', '修改会员状态', '37', '1', 'admin/user/updateUserStatus', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('44', '导出会员信息', '37', '1', 'admin/user/userExcel', '0', 'per', null);
INSERT INTO `auth_menu` VALUES ('45', '系统统计', '0', '1', null, null, 'menu', '&#xe61a;');
INSERT INTO `auth_menu` VALUES ('46', '会员统计', '45', '1', 'admin/echarts/user', null, 'menu', '');

-- ----------------------------
-- Table structure for auth_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_role`;
CREATE TABLE `auth_role` (
  `role_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `role_name` varchar(100) NOT NULL DEFAULT '' COMMENT '角色名称',
  `desc` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `menu_id` varchar(255) NOT NULL COMMENT '菜单栏id',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- ----------------------------
-- Records of auth_role
-- ----------------------------
INSERT INTO `auth_role` VALUES ('1', '超级管理员', '超级管理员', '36,37,39,40,41,43,44,38,42,45,46,1,3,11,12,13,4,14,15,16,5,7,8,9,10,2,6,17,18,20,26,32,34,35', '2018-10-26 10:33:12');
INSERT INTO `auth_role` VALUES ('2', '操作员', '操作员', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,26,32,34,35,36,37,38,39,40,41,42,43,44', '2018-10-26 10:25:23');
INSERT INTO `auth_role` VALUES ('3', '都看', '都看', '', '2018-10-12 15:37:32');
INSERT INTO `auth_role` VALUES ('4', '游客', '游客', '', '2018-10-12 15:37:30');

-- ----------------------------
-- Table structure for operation
-- ----------------------------
DROP TABLE IF EXISTS `operation`;
CREATE TABLE `operation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL COMMENT '操作管理员',
  `action` varchar(255) DEFAULT NULL COMMENT '操作方法',
  `time` int(11) DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of operation
-- ----------------------------
INSERT INTO `operation` VALUES ('900', 'admin', 'admin/echarts/user', '1540523865');
INSERT INTO `operation` VALUES ('901', 'admin', 'admin/echarts/user', '1540523884');
INSERT INTO `operation` VALUES ('902', 'admin', 'admin/echarts/user', '1540523960');
INSERT INTO `operation` VALUES ('903', 'admin', 'admin/echarts/user', '1540523988');
INSERT INTO `operation` VALUES ('904', 'admin', 'admin/echarts/user', '1540524062');
INSERT INTO `operation` VALUES ('905', 'admin', 'admin/echarts/user', '1540524253');
INSERT INTO `operation` VALUES ('906', 'admin', 'admin/echarts/user', '1540524289');
INSERT INTO `operation` VALUES ('907', 'admin', 'admin/echarts/user', '1540524335');
INSERT INTO `operation` VALUES ('908', 'admin', 'admin/echarts/user', '1540524346');
INSERT INTO `operation` VALUES ('909', 'admin', 'admin/echarts/user', '1540524967');
INSERT INTO `operation` VALUES ('910', 'admin', 'admin/echarts/user', '1540526170');
INSERT INTO `operation` VALUES ('911', 'admin', 'admin/echarts/user', '1540526277');
INSERT INTO `operation` VALUES ('912', 'admin', 'admin/echarts/user', '1540530860');
INSERT INTO `operation` VALUES ('913', 'admin', 'admin/echarts/user', '1540530867');
INSERT INTO `operation` VALUES ('914', 'admin', 'admin/echarts/user', '1540530893');
INSERT INTO `operation` VALUES ('915', 'admin', 'admin/echarts/user', '1540531006');
INSERT INTO `operation` VALUES ('916', 'admin', 'admin/echarts/user', '1540531040');
INSERT INTO `operation` VALUES ('917', 'admin', 'admin/echarts/user', '1540531074');
INSERT INTO `operation` VALUES ('918', 'admin', 'admin/echarts/user', '1540531169');
INSERT INTO `operation` VALUES ('919', 'admin', 'admin/echarts/user', '1540531180');
INSERT INTO `operation` VALUES ('920', 'admin', 'admin/echarts/user', '1540531260');
INSERT INTO `operation` VALUES ('921', 'admin', 'admin/echarts/user', '1540531268');
INSERT INTO `operation` VALUES ('922', 'admin', 'admin/echarts/user', '1540531286');
INSERT INTO `operation` VALUES ('923', 'admin', 'admin/echarts/user', '1540531333');
INSERT INTO `operation` VALUES ('924', 'admin', 'admin/echarts/user', '1540531477');
INSERT INTO `operation` VALUES ('925', null, 'admin/index/index', '1540531632');
INSERT INTO `operation` VALUES ('926', 'admin', 'admin/index/index', '1540531771');
INSERT INTO `operation` VALUES ('927', 'admin', 'admin/index/welcome', '1540531773');
INSERT INTO `operation` VALUES ('928', 'admin', 'admin/echarts/user', '1540531775');
INSERT INTO `operation` VALUES ('929', 'admin', 'admin/echarts/user', '1540531814');
INSERT INTO `operation` VALUES ('930', 'admin', 'admin/echarts/user', '1540531868');
INSERT INTO `operation` VALUES ('931', 'admin', 'admin/echarts/user', '1540531943');
INSERT INTO `operation` VALUES ('932', 'admin', 'admin/echarts/user', '1540532019');
INSERT INTO `operation` VALUES ('933', 'admin', 'admin/echarts/user', '1540532020');
INSERT INTO `operation` VALUES ('934', 'admin', 'admin/echarts/user', '1540532021');
INSERT INTO `operation` VALUES ('935', 'admin', 'admin/echarts/user', '1540532022');
INSERT INTO `operation` VALUES ('936', 'admin', 'admin/echarts/user', '1540532042');
INSERT INTO `operation` VALUES ('937', 'admin', 'admin/echarts/user', '1540532075');
INSERT INTO `operation` VALUES ('938', 'admin', 'admin/echarts/user', '1540532098');
INSERT INTO `operation` VALUES ('939', 'admin', 'admin/echarts/user', '1540532121');
INSERT INTO `operation` VALUES ('940', 'admin', 'admin/echarts/user', '1540532121');
INSERT INTO `operation` VALUES ('941', 'admin', 'admin/echarts/user', '1540532221');
INSERT INTO `operation` VALUES ('942', 'admin', 'admin/echarts/user', '1540532225');
INSERT INTO `operation` VALUES ('943', 'admin', 'admin/echarts/user', '1540532226');
INSERT INTO `operation` VALUES ('944', 'admin', 'admin/echarts/user', '1540532226');
INSERT INTO `operation` VALUES ('945', 'admin', 'admin/echarts/user', '1540532229');
INSERT INTO `operation` VALUES ('946', 'admin', 'admin/echarts/user', '1540532360');
INSERT INTO `operation` VALUES ('947', 'admin', 'admin/echarts/user', '1540532377');
INSERT INTO `operation` VALUES ('948', 'admin', 'admin/echarts/user', '1540532397');
INSERT INTO `operation` VALUES ('949', 'admin', 'admin/echarts/user', '1540532407');
INSERT INTO `operation` VALUES ('950', 'admin', 'admin/echarts/user', '1540532423');
INSERT INTO `operation` VALUES ('951', 'admin', 'admin/echarts/user', '1540532424');
INSERT INTO `operation` VALUES ('952', 'admin', 'admin/echarts/user', '1540532503');
INSERT INTO `operation` VALUES ('953', 'admin', 'admin/echarts/user', '1540532530');
INSERT INTO `operation` VALUES ('954', 'admin', 'admin/echarts/user', '1540533004');
INSERT INTO `operation` VALUES ('955', 'admin', 'admin/echarts/user', '1540533374');
INSERT INTO `operation` VALUES ('956', 'admin', 'admin/echarts/user', '1540533375');
INSERT INTO `operation` VALUES ('957', 'admin', 'admin/echarts/user', '1540534161');
INSERT INTO `operation` VALUES ('958', 'admin', 'admin/echarts/user', '1540534260');
INSERT INTO `operation` VALUES ('959', 'admin', 'admin/echarts/user', '1540534286');
INSERT INTO `operation` VALUES ('960', 'admin', 'admin/echarts/user', '1540535040');
INSERT INTO `operation` VALUES ('961', 'admin', 'admin/echarts/user', '1540535045');
INSERT INTO `operation` VALUES ('962', 'admin', 'admin/echarts/user', '1540535106');
INSERT INTO `operation` VALUES ('963', 'admin', 'admin/echarts/user', '1540535199');
INSERT INTO `operation` VALUES ('964', 'admin', 'admin/echarts/user', '1540535220');
INSERT INTO `operation` VALUES ('965', 'admin', 'admin/echarts/user', '1540535230');
INSERT INTO `operation` VALUES ('966', 'admin', 'admin/echarts/user', '1540535241');
INSERT INTO `operation` VALUES ('967', 'admin', 'admin/echarts/user', '1540535281');
INSERT INTO `operation` VALUES ('968', 'admin', 'admin/echarts/user', '1540535345');
INSERT INTO `operation` VALUES ('969', 'admin', 'admin/echarts/user', '1540535376');
INSERT INTO `operation` VALUES ('970', 'admin', 'admin/echarts/user', '1540535396');
INSERT INTO `operation` VALUES ('971', 'admin', 'admin/echarts/user', '1540535397');
INSERT INTO `operation` VALUES ('972', 'admin', 'admin/echarts/user', '1540535434');
INSERT INTO `operation` VALUES ('973', 'admin', 'admin/echarts/user', '1540535559');
INSERT INTO `operation` VALUES ('974', 'admin', 'admin/echarts/user', '1540535913');
INSERT INTO `operation` VALUES ('975', 'admin', 'admin/echarts/user', '1540535938');
INSERT INTO `operation` VALUES ('976', 'admin', 'admin/echarts/user', '1540536087');
INSERT INTO `operation` VALUES ('977', 'admin', 'admin/echarts/user', '1540536192');
INSERT INTO `operation` VALUES ('978', 'admin', 'admin/echarts/user', '1540536208');
INSERT INTO `operation` VALUES ('979', 'admin', 'admin/echarts/user', '1540536225');
INSERT INTO `operation` VALUES ('980', 'admin', 'admin/echarts/user', '1540536275');
INSERT INTO `operation` VALUES ('981', 'admin', 'admin/echarts/user', '1540536303');
INSERT INTO `operation` VALUES ('982', 'admin', 'admin/echarts/user', '1540536349');
INSERT INTO `operation` VALUES ('983', 'admin', 'admin/echarts/user', '1540536379');
INSERT INTO `operation` VALUES ('984', 'admin', 'admin/echarts/user', '1540536410');
INSERT INTO `operation` VALUES ('985', 'admin', 'admin/echarts/user', '1540536662');
INSERT INTO `operation` VALUES ('986', 'admin', 'admin/echarts/user', '1540536711');
INSERT INTO `operation` VALUES ('987', 'admin', 'admin/echarts/user', '1540536745');
INSERT INTO `operation` VALUES ('988', 'admin', 'admin/echarts/user', '1540536762');
INSERT INTO `operation` VALUES ('989', 'admin', 'admin/user/index', '1540537404');
INSERT INTO `operation` VALUES ('990', 'admin', 'admin/user/edituser', '1540537411');
INSERT INTO `operation` VALUES ('991', 'admin', 'admin/user/updateuserstatus', '1540537420');
INSERT INTO `operation` VALUES ('992', 'admin', 'admin/user/adduser', '1540537424');
INSERT INTO `operation` VALUES ('993', 'admin', 'admin/user/adduser', '1540537456');
INSERT INTO `operation` VALUES ('994', 'admin', 'admin/user/adduser', '1540537458');
INSERT INTO `operation` VALUES ('995', 'admin', 'admin/user/index', '1540537459');
INSERT INTO `operation` VALUES ('996', 'admin', 'admin/echarts/user', '1540537467');
INSERT INTO `operation` VALUES ('997', 'admin', 'admin/echarts/user', '1540537517');
INSERT INTO `operation` VALUES ('998', 'admin', 'admin/echarts/user', '1540537535');
INSERT INTO `operation` VALUES ('999', 'admin', 'admin/echarts/user', '1540538147');
INSERT INTO `operation` VALUES ('1000', 'admin', 'admin/user/index', '1540538172');
INSERT INTO `operation` VALUES ('837', 'admin', 'admin/index/index', '1540520468');
INSERT INTO `operation` VALUES ('838', 'admin', 'admin/index/welcome', '1540520470');
INSERT INTO `operation` VALUES ('839', 'admin', 'admin/admin/permission', '1540520474');
INSERT INTO `operation` VALUES ('840', 'admin', 'admin/menu/index', '1540520487');
INSERT INTO `operation` VALUES ('841', 'admin', 'admin/index/index', '1540520735');
INSERT INTO `operation` VALUES ('842', 'admin', 'admin/index/welcome', '1540520736');
INSERT INTO `operation` VALUES ('843', 'admin', 'admin/user/index', '1540520739');
INSERT INTO `operation` VALUES ('844', 'admin', 'admin/user/recovery', '1540520742');
INSERT INTO `operation` VALUES ('845', 'admin', 'admin/admin/role', '1540520746');
INSERT INTO `operation` VALUES ('846', 'admin', 'admin/admin/permission', '1540520747');
INSERT INTO `operation` VALUES ('847', 'admin', 'admin/admin/editpermission', '1540520791');
INSERT INTO `operation` VALUES ('848', 'admin', 'admin/admin/editpermission', '1540520813');
INSERT INTO `operation` VALUES ('849', 'admin', 'admin/menu/index', '1540520860');
INSERT INTO `operation` VALUES ('850', 'admin', 'admin/system/operation', '1540520863');
INSERT INTO `operation` VALUES ('851', 'admin', 'admin/system/operation', '1540520901');
INSERT INTO `operation` VALUES ('852', 'admin', 'admin/menu/index', '1540520919');
INSERT INTO `operation` VALUES ('853', 'admin', 'admin/menu/addmenu', '1540520922');
INSERT INTO `operation` VALUES ('854', 'admin', 'admin/menu/addmenu', '1540521033');
INSERT INTO `operation` VALUES ('855', 'admin', 'admin/menu/index', '1540521035');
INSERT INTO `operation` VALUES ('856', 'admin', 'admin/admin/permission', '1540521044');
INSERT INTO `operation` VALUES ('857', 'admin', 'admin/admin/addpermission', '1540521047');
INSERT INTO `operation` VALUES ('858', 'admin', 'admin/menu/index', '1540521059');
INSERT INTO `operation` VALUES ('859', 'admin', 'admin/menu/addmenu', '1540521061');
INSERT INTO `operation` VALUES ('860', 'admin', 'admin/menu/addmenu', '1540521167');
INSERT INTO `operation` VALUES ('861', 'admin', 'admin/menu/index', '1540521170');
INSERT INTO `operation` VALUES ('862', 'admin', 'admin/admin/role', '1540521184');
INSERT INTO `operation` VALUES ('863', 'admin', 'admin/admin/editrole', '1540521187');
INSERT INTO `operation` VALUES ('864', 'admin', 'admin/admin/editrole', '1540521192');
INSERT INTO `operation` VALUES ('865', 'admin', 'admin/admin/role', '1540521195');
INSERT INTO `operation` VALUES ('866', 'admin', 'admin/index/index', '1540521204');
INSERT INTO `operation` VALUES ('867', 'admin', 'admin/index/welcome', '1540521205');
INSERT INTO `operation` VALUES ('868', 'admin', 'admin/admin/permission', '1540522441');
INSERT INTO `operation` VALUES ('869', 'admin', 'admin/admin/index', '1540522451');
INSERT INTO `operation` VALUES ('870', 'admin', 'admin/menu/index', '1540522461');
INSERT INTO `operation` VALUES ('871', 'admin', 'admin/echarts/user', '1540522515');
INSERT INTO `operation` VALUES ('872', 'admin', 'admin/echarts/user', '1540523040');
INSERT INTO `operation` VALUES ('873', 'admin', 'admin/echarts/user', '1540523042');
INSERT INTO `operation` VALUES ('874', 'admin', 'admin/echarts/user', '1540523053');
INSERT INTO `operation` VALUES ('875', 'admin', 'admin/echarts/user', '1540523267');
INSERT INTO `operation` VALUES ('876', 'admin', 'admin/echarts/user', '1540523269');
INSERT INTO `operation` VALUES ('877', 'admin', 'admin/echarts/user', '1540523290');
INSERT INTO `operation` VALUES ('878', 'admin', 'admin/echarts/user', '1540523291');
INSERT INTO `operation` VALUES ('879', 'admin', 'admin/echarts/user', '1540523292');
INSERT INTO `operation` VALUES ('880', 'admin', 'admin/echarts/user', '1540523292');
INSERT INTO `operation` VALUES ('881', 'admin', 'admin/echarts/user', '1540523293');
INSERT INTO `operation` VALUES ('882', 'admin', 'admin/echarts/user', '1540523293');
INSERT INTO `operation` VALUES ('883', 'admin', 'admin/echarts/user', '1540523364');
INSERT INTO `operation` VALUES ('884', 'admin', 'admin/echarts/user', '1540523554');
INSERT INTO `operation` VALUES ('885', 'admin', 'admin/echarts/user', '1540523555');
INSERT INTO `operation` VALUES ('886', 'admin', 'admin/echarts/user', '1540523596');
INSERT INTO `operation` VALUES ('887', 'admin', 'admin/echarts/user', '1540523597');
INSERT INTO `operation` VALUES ('888', 'admin', 'admin/echarts/user', '1540523598');
INSERT INTO `operation` VALUES ('889', 'admin', 'admin/echarts/user', '1540523629');
INSERT INTO `operation` VALUES ('890', 'admin', 'admin/index/index', '1540523651');
INSERT INTO `operation` VALUES ('891', 'admin', 'admin/index/welcome', '1540523652');
INSERT INTO `operation` VALUES ('892', 'admin', 'admin/echarts/user', '1540523658');
INSERT INTO `operation` VALUES ('893', 'admin', 'admin/echarts/user', '1540523709');
INSERT INTO `operation` VALUES ('894', 'admin', 'admin/echarts/user', '1540523723');
INSERT INTO `operation` VALUES ('895', 'admin', 'admin/echarts/user', '1540523735');
INSERT INTO `operation` VALUES ('896', 'admin', 'admin/index/index', '1540523774');
INSERT INTO `operation` VALUES ('897', 'admin', 'admin/index/welcome', '1540523776');
INSERT INTO `operation` VALUES ('898', 'admin', 'admin/echarts/user', '1540523782');
INSERT INTO `operation` VALUES ('899', 'admin', 'admin/echarts/user', '1540523840');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) DEFAULT NULL COMMENT '会员的名称',
  `password` varchar(120) DEFAULT NULL COMMENT '会员密码',
  `email` varchar(255) DEFAULT NULL COMMENT '会员的邮箱',
  `phone` varchar(120) DEFAULT NULL COMMENT '电话',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `user_logo` varchar(255) DEFAULT NULL COMMENT '会员头像',
  `last_ip` varchar(255) DEFAULT NULL COMMENT '用户登录的ip',
  `last_time` int(11) DEFAULT NULL COMMENT '会员登录的时间',
  `add_time` int(11) DEFAULT NULL COMMENT '添加时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新的时间',
  `status` tinyint(2) DEFAULT NULL COMMENT '会员状态 0 禁用 1 启用',
  `rective` tinyint(2) DEFAULT '1' COMMENT '会员删除   0 删除  1恢复',
  `user_browser` varchar(255) DEFAULT NULL COMMENT '用户使用的设备',
  `user_system` varchar(255) DEFAULT NULL COMMENT '系统',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('9', '2222', '$2y$10$IZeDMGnZp8Z0UyPLAKb4xOscHpYOxHS2nHiahL7apAMHAM37hukIq', 'admin@qq.com', '2222', '30', '/upload/user/2018/10-25/ac97a814bc500613ac2b73ccdfae5e85.jpg', '127.0.0.1', '1540453861', '1540453861', '1540453861', '1', '1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36', 'Windows');
INSERT INTO `user` VALUES ('10', '1111', '$2y$10$WnTGZ1ukcssPe1.tJJprguW1pTheWh331Sm7Yqa/VpAr1iDwZiUVi', 'admin@qq.com', '11111', '20', '/upload/user/2018/10-25/570947e78f36b236bcc71873703c2817.jpg', '127.0.0.1', '1540453811', '1540453811', '1540453811', '1', '1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36', 'Windows');
INSERT INTO `user` VALUES ('11', '55', '$2y$10$WnTGZ1ukcssPe1.tJJprguW1pTheWh331Sm7Yqa/VpAr1iDwZiUVi', 'admin@qq.com', '11111', '36', '/upload/user/2018/10-25/570947e78f36b236bcc71873703c2817.jpg', '127.0.0.1', '1540453811', '1540453811', '1540453811', '1', '1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36', 'Linux');
INSERT INTO `user` VALUES ('12', '36', '$2y$10$WnTGZ1ukcssPe1.tJJprguW1pTheWh331Sm7Yqa/VpAr1iDwZiUVi', 'admin@qq.com', '11111', '20', '/upload/user/2018/10-25/570947e78f36b236bcc71873703c2817.jpg', '127.0.0.1', '1540453811', '1540453811', '1540453811', '1', '1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36', 'Mac');
INSERT INTO `user` VALUES ('13', '666', '$2y$10$WnTGZ1ukcssPe1.tJJprguW1pTheWh331Sm7Yqa/VpAr1iDwZiUVi', 'admin@qq.com', '11111', '25', '/upload/user/2018/10-25/570947e78f36b236bcc71873703c2817.jpg', '127.0.0.1', '1540453811', '1540453811', '1540453811', '1', '1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36', 'Linux');
INSERT INTO `user` VALUES ('14', '888', '$2y$10$WnTGZ1ukcssPe1.tJJprguW1pTheWh331Sm7Yqa/VpAr1iDwZiUVi', 'admin@qq.com', '11111', '20', '/upload/user/2018/10-25/570947e78f36b236bcc71873703c2817.jpg', '127.0.0.1', '1540453811', '1540453811', '1540453811', '1', '1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36', 'Mac');
INSERT INTO `user` VALUES ('15', '888', '$2y$10$WnTGZ1ukcssPe1.tJJprguW1pTheWh331Sm7Yqa/VpAr1iDwZiUVi', 'admin@qq.com', '11111', '16', '/upload/user/2018/10-25/570947e78f36b236bcc71873703c2817.jpg', '127.0.0.1', '1540453811', '1540453811', '1540453811', '1', '1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36', 'Linux');
INSERT INTO `user` VALUES ('16', '666', '$2y$10$WnTGZ1ukcssPe1.tJJprguW1pTheWh331Sm7Yqa/VpAr1iDwZiUVi', 'admin@qq.com', '11111', '28', '/upload/user/2018/10-25/570947e78f36b236bcc71873703c2817.jpg', '127.0.0.1', '1540453811', '1540453811', '1540453811', '1', '1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36', 'Mac');
INSERT INTO `user` VALUES ('17', '小和', '$2y$10$P/bp5suJfH1XpgdowK8UY.3anLo4a8YD/cF5I/sW0/6p1lZU.s1QK', 'admin@qq.com', '153112356532', '21', '/upload/user/2018/10-26/8655a30ab629d1f1750584f620ba2a27.jpg', '127.0.0.1', '1540537456', '1540537456', '1540537456', '1', '1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36', 'Windows');
