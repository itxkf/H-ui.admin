<?php

namespace app\admin\model;

use think\Db;
use think\Model;

class UserModel extends Model
{
    protected $table="user";

    /*
     *查询所有
     */
    public function user_list($where,$min,$max){
        if ($min && $max){
            $result=Db::name("user")
                ->where($where)
                ->whereTime("add_time","between",[$min,$max])
                ->where("rective",1)
                ->select();
            return $result;
        }else{
            $result=Db::name("user")
                ->where($where)
                ->where("rective",1)
                ->select();
            return $result;
        }
    }

    /*
     * 删除
     */
    public function del_user($id){
        $user=UserModel::get($id);
        if(empty($user)){
            return array("code"=>0,"msg"=>"信息有误");
        }
        $data=Db::name("user")->where("id",$id)->update(["status"=>0,"rective"=>0]);
        if ($data){
            return array("code"=>1,"msg"=>"删除成功");
        }else{
            return array("code"=>0,"msg"=>"删除失败");
        }
    }
    /*
     * 修改会员的状态
     */
    public function update_user_status($id,$status){
        $info=UserModel::get($id);
        if(empty($info)){
            return array("code"=>0,"msg"=>"信息有误");
        }
        $result=Db::name("user")->where(array("id"=>$id))->update(array("status"=>$status));
        if($result){
            return array("code"=>1,"msg"=>"修改成功");
        }else{
            return array("code"=>0,"msg"=>"修改失败");
        }
    }

    /*
     * 读取回收的会员
     */
    public function recovery_list($where,$min,$max){
        if ($min && $max){
            $result=Db::name("user")
                ->where($where)
                ->whereTime("add_time","between",[$min,$max])
                ->where("rective",0)
                ->select();
            return $result;
        }else{
            $result=Db::name("user")
                ->where($where)
                ->where("rective",0)
                ->select();
            return $result;
        }
    }
    /*
     * 还原会员
     */
    public function reduction_user($id){
        $user=UserModel::get($id);
        if(empty($user)){
            return array("code"=>0,"msg"=>"信息有误");
        }
        $data=Db::name("user")->where("id",$id)->update(["status"=>1,"rective"=>1]);
        if ($data){
            return array("code"=>1,"msg"=>"还原成功");
        }else{
            return array("code"=>0,"msg"=>"还原失败");
        }
    }
}
