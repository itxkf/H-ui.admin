<?php

namespace app\admin\controller;

use app\admin\model\UserModel;
use think\Controller;
use think\Cookie;
use think\Db;
use think\Request;
use think\Session;

class User extends Base
{
    /*
     * 列表页
     */
    public function index(){
        $min=isset($_POST["logmin"])?strtotime($_POST["logmin"]):"";
        $max=isset($_POST["logmax"])?strtotime($_POST["logmax"]):"";
        $keyword=isset($_POST['keyword'])?$_POST['keyword']:'';
        $where=null;
        if (!empty($keyword)){
            $where["u.names"]=array("like","%$keyword%");
        }
        $user=new UserModel();
        $data=$user->user_list($where,$min,$max);
        $num=count($data);
        $this->assign("data",$data);
        $this->assign("num",$num);
        return $this->fetch("user-list");
    }

    /*
     * 会员的添加
     */
    public function addUser(){
        if(\request()->isPost()){
            $insert_array=array();
            $insert_array["user_name"]=input("user_name");
            $insert_array["password"]=password_hash(input("password"),1);
            $insert_array["age"]=input("age");
            $insert_array["email"]=input("email");
            $insert_array["phone"]=input("phone");
            $insert_array["status"]=input("status");
            $insert_array["last_time"]=time();
            $insert_array["last_ip"]=\request()->ip();
            $insert_array["add_time"]=time();
            $insert_array["update_time"]=time();
            $browser=$_SERVER['HTTP_USER_AGENT'];
            $insert_array["user_browser"]=$browser;
            if (!empty($browser)) {
                if (preg_match('/win/i', $browser)) {
                    $insert_array["user_system"]="Windows";
                } else if (preg_match('/mac/i', $browser)) {
                    $insert_array["user_system"]="Mac";
                } else if (preg_match('/linux/i', $browser)) {
                    $insert_array["user_system"]="Linux";
                } else if (preg_match('/unix/i', $browser)) {
                    $insert_array["user_system"]="Unix";
                } else if (preg_match('/bsd/i', $browser)) {
                    $insert_array["user_system"]="BSD";
                } else {
                    $insert_array["user_system"]="没有次系统";
                }
            } else {
                return 'unknow';
            }

            $file=\request()->file("file");
            if ($file){
                $imgPath="";
                //移动文件位置
                if ($file){
                    $info = $file->move(ROOT_PATH . 'public' . DS . 'upload' . DS . 'user' . DS . date('Y') . DS . date('m-d'),md5(microtime(true)));
                    if ($info) {
                        // 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
                        $imgPath = "/upload/user/" . date('Y') . '/' . date('m-d') . '/' . $info->getSaveName();
                    }
                }else{
                    return $this->error($file->getError());
                }
                //赋值
                $insert_array["user_logo"]=$imgPath;
                $result=Db::name("user")->insert($insert_array);
                if ($result){
                    $this->success("添加成功");
                }else{
                    $this->error("添加失败");
                }
            }else{
                $this->error("请添加图片");
            }
        }
        return $this->fetch("user-add");
    }
    /*
     * 会员的编辑
     */
     public function editUser(){
         $id=input("id");
         $data=UserModel::get($id);
         if (empty($id)){
             $this->error("参数错误");
         }
        if(\request()->isPost()){
            $insert_array=array();
            $insert_array["user_name"]=input("user_name");
            $insert_array["password"]=password_hash(input("password"),1);
            $insert_array["age"]=input("age");
            $insert_array["email"]=input("email");
            $insert_array["phone"]=input("phone");
            $insert_array["status"]=input("status");
            $insert_array["last_time"]=time();
            $insert_array["last_ip"]=\request()->ip();
            $insert_array["add_time"]=time();
            $insert_array["update_time"]=time();
            $browser=$_SERVER['HTTP_USER_AGENT'];
            $insert_array["user_browser"]=$browser;
            if (!empty($browser)) {
                if (preg_match('/win/i', $browser)) {
                    $insert_array["user_system"]="Windows";
                } else if (preg_match('/mac/i', $browser)) {
                    $insert_array["user_system"]="Mac";
                } else if (preg_match('/linux/i', $browser)) {
                    $insert_array["user_system"]="Linux";
                } else if (preg_match('/unix/i', $browser)) {
                    $insert_array["user_system"]="Unix";
                } else if (preg_match('/bsd/i', $browser)) {
                    $insert_array["user_system"]="BSD";
                } else {
                    $insert_array["user_system"]="没有次系统";
                }
            } else {
                return 'unknow';
            }
            $file=\request()->file("file");
            if ($file){
                $imgPath="";
                //移动文件位置
                if ($file){
                    $info = $file->move(ROOT_PATH . 'public' . DS . 'upload' . DS . 'user' . DS . date('Y') . DS . date('m-d'),md5(microtime(true)));
                    if ($info) {
                        // 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
                        $imgPath = "/upload/user/" . date('Y') . '/' . date('m-d') . '/' . $info->getSaveName();
                    }
                }else{
                    return $this->error($file->getError());
                }
                //赋值
                $insert_array["user_logo"]=$imgPath;
                $result=Db::name("user")->where("id",$id)->update($insert_array);
                if ($result){
                    $this->success("编辑成功");
                }else{
                    $this->error("编辑失败");
                }
            }else{
                $insert_array["user_logo"]=$data["user_logo"];
                $result=Db::name("user")->where("id",$id)->update($insert_array);
                if ($result){
                    $this->success("编辑成功");
                }else{
                    $this->error("编辑失败");
                }
            }
        }

        $this->assign("data",$data);
        return $this->fetch("user-edit");
    }
    /*
     * 删除会员
     */
    public function delUser(){
        $id=input("id");
        $user=new UserModel();
        $data=$user->del_user($id);
        if($data["code"]==1){
            $this->success($data["msg"]);
        }else{
            $this->error($data["msg"]);
        }
    }

    /*
     *修改状态
     */
    public function updateUserStatus(){
        $id=input("id");
        $status=input("status");
        $user=new UserModel();
        $data=$user->update_user_status($id,$status);
        if ($data["code"]==1){
            $this->success($data["msg"]);
        }else{
            $this->error($data["msg"]);
        }
    }

    /*
     * 会员回收站
     */
    public function recovery(){
        $min=isset($_POST["logmin"])?strtotime($_POST["logmin"]):"";
        $max=isset($_POST["logmax"])?strtotime($_POST["logmax"]):"";
        $keyword=isset($_POST['keyword'])?$_POST['keyword']:'';
        $where=null;
        if (!empty($keyword)){
            $where["u.names"]=array("like","%$keyword%");
        }
        $user=new UserModel();
        $data=$user->recovery_list($where,$min,$max);
        $num=count($data);
        $this->assign("data",$data);
        $this->assign("num",$num);
        return $this->fetch("user-del");
    }

    /*
     * 会员还原
     */
    public function reduction(){
        $id=input("id");
        $user=new UserModel();
        $data=$user->reduction_user($id);
        if ($data["code"]==1){
            $this->success($data["msg"]);
        }else{
            $this->error($data["msg"]);
        }
    }
    /*
     * 导出会员信息
     */
    public function userExcel(){

        $users = UserModel::all();     //数据库查询
        $path = dirname(__FILE__); //找到当前脚本所在路径

        vendor("PHPExcel.PHPExcel"); //方法一

        $PHPExcel = new \PHPExcel();
        $PHPSheet = $PHPExcel->getActiveSheet();
        $PHPSheet->setTitle("demo"); //给当前活动sheet设置名称
        $PHPSheet->setCellValue("A1", "ID")
            ->setCellValue("B1", "用户名")
            ->setCellValue("C1", "邮箱")
            ->setCellValue("D1", "手机")
            ->setCellValue("E1", "头像地址")
            ->setCellValue("F1", "登录ip")
            ->setCellValue("G1", "登录时间");
        $i = 2;
        foreach($users as $data){
            $PHPSheet->setCellValue("A" . $i, $data['id'])
                ->setCellValue("B" . $i, $data['user_name'])
                ->setCellValue("C" . $i, $data['email'])
                ->setCellValue("D" . $i, $data['phone'])
                ->setCellValue("E" . $i, $data['user_logo'])
                ->setCellValue("F" . $i, $data['last_ip'])
                ->setCellValue("G" . $i, $data['last_time']);
            $i++;
        }

        $PHPWriter = \PHPExcel_IOFactory::createWriter($PHPExcel, "Excel2007");
        header('Content-Disposition: attachment;filename="会员表单数据.xlsx"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $PHPWriter->save("php://output"); //表示在$path路径下面生成demo.xlsx文件
    }
}
