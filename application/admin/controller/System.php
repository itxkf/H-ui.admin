<?php

namespace app\admin\controller;

use app\admin\model\OperationModel;
use think\Controller;
use think\Request;

class System extends Base
{
    /**
     * 显示资源列表   操作日志
     */
    public function operation()
    {
        $min=isset($_POST["logmin"])?strtotime($_POST["logmin"]):"";
        $max=isset($_POST["logmax"])?strtotime($_POST["logmax"]):"";
        $keyword=isset($_POST["keyword"])?$_POST["keyword"]:"";
        $where = null;
        if(!empty($keyword)){
            $where['name'] = array("like","%$keyword%");
        }
        $model=new OperationModel();
        $data=$model->Loglist($where,$min,$max);
        $num=count($data);
        $this->assign("data",$data);
        $this->assign("num",$num);
        //
        return $this->fetch("system-operation");

    }

    /*
     * 批量删除  操作日志
     */
    public function delOperation(){
        $id=input("id/a");
        $model=new OperationModel();
        $data=$model->delAll($id);
        if ($data["code"]==1){
            $this->success($data["msg"]);
        }else{
            $this->error($data["msg"]);
        }

    }

}
