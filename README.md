# H-ui.admin
#### 免责声明
1-该系统只使用学习或者参考，不具备商业用途。

2-如果出现问题与本人无关，

3-如因本人发布的项目内容涉及版权或存在其他问题，请于联系我进行删除。


#### 项目介绍
基于thinkphp5 做的后台管理系统 包含了权限管理以及菜单栏管理 一些常用的功能。 
 超级管理员   账号和密码 admin 6666   常用的基础模块都存在的
如下
![输入图片说明](https://images.gitee.com/uploads/images/2019/0520/142856_e1d10ee5_1921585.png "在这里输入图片标题")
![输入图片说明](https://box.kancloud.cn/c980a777623b756aa549593b6d8fd5e7_1878x854.png "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0520/142901_1e1beae9_1921585.png "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0520/142600_c248ea70_1921585.png "在这里输入图片标题")
![输入图片说明](https://box.kancloud.cn/9297df0c6bad415e2432bcc01360960c_1653x792.png "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0520/142600_9f152d73_1921585.png "在这里输入图片标题")
![输入图片说明](https://box.kancloud.cn/dc8b3f054e9c995f1f3ab92213a7758c_1877x835.png "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0520/142856_a5f38b1e_1921585.png "在这里输入图片标题")
#### 软件架构
软件架构说明


#### 安装教程

将代码下载下来    数据库的文件在项目根目录的data（文件夹下）  导入数据库

#### 使用说明
进入public文件夹下   shift+右键  点击在此处打开命令窗口   输入 php -S 0.0.0.0：99  当然端口号可以随便取
  在浏览器输入  127.0.0.1:99/模块/控制器/方法    就可以使用    

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)


#### 若对您有帮助，可以赞助并支持下作者哦，谢谢！ 或者给个 star   谢谢你的支持 



![输入图片说明](https://images.gitee.com/uploads/images/2018/0907/164258_64e74acd_1921585.jpeg "微信图片_20180907163903.jpg")